**Gesammelte Informationen zur Zielsetzung, Rahmenbedingungen und Bewerbung zum begleitenden Konsultationsprozess zur Zielbilderarbeitung der OZG-Rahmenarchitektur durch das Föderale IT-Architekturboard finden Sie hier:** [Informationsseite Zielbild OZG-Rahmenarchitektur Konsultation](https://bmi.usercontent.opencode.de/ozg-rahmenarchitektur/zielbild-ozg-rahmenarchitektur/)

## Aktuelles
- **NEU** 
Den Ergebnisbericht zur ersten Phase des Konsultationsprozesses finden Sie [hier](https://www.digitale-verwaltung.de/SharedDocs/downloads/Webs/DV/DE/ergbnisprotokoll_konsultationsprozess_phase_1.pdf?__blob=publicationFile&v=3)

## Zeitplan des Konsultationsprozess

![Fahrplan](Konsultationsprozess_Fahrplan.png)

Die Auftaktveranstaltung zum Konsultationsprozess findet am 24. Oktober 2023 statt. Informationen zum weiteren Prozess, einschließlich der geplanten Termine für die themenspezifischen Online-Webseminare, sind dem beigefügten Zeitplan zu entnehmen.

> **Hinweis** Die genannten Termine sind zunächst als "Save the Date" zu verstehen. Weitere Veranstaltungsformate und Termine werden nach Bedarf ergänzt und entsprechend kommuniziert.

**September 2023**  

- [x] 20.09.      Start der Bewerbungsphase

**Oktober 2023**

- [x] 20.10.23    Ende der Bewerbungsphase
- [x] 24.10.23    Informationsveranstaltung I - Webseminar (15:30-17:00)
- [x] 26.10.23    Start Online-Konsultation

**November 2023**   

- [x] 20.11.23    Rückmeldung zu den Leitfragen aus dem Release # 1 und # 2

**Dezember 2023**   

- [x] 05.12.23    Informationsveranstaltung II - Webseminar (15:30-17:00)

**Januar 2024** 

- [x] 08.01.23    Rückmeldung zu den Leitfragen aus dem Release # 3
- [x] 18.01.24    Informationsveranstaltung III - Webseminar (15:30-17:00)
- [x] 28.01.24    Ende Online-Konsultation

**Februar 2024** 

**März 2024**     
- [ ]             Ergebnisbericht zu dem Konsultationsprozess


## Beteiligen
Für die aktive Beteiligung im Konsultationsprozess zur Erarbeitung des Zielbildes der OZG-Rahmenarchitektur, folgen Sie der untenstehenden Schritt-für-Schritt Anleitung. <br>

> **Hinweis:** Die Beiträge und Kommentare in diesem Projekt sind für alle Teilnehmenden des Konsultationsprozesses einsehbar und können perspektivisch veröffentlicht werden.


### Schritt 1: Sichtung der veröffentlichten Beiträge

![Issue Board](Anleitung_Sichtung.png)

Bitte prüfen Sie die Leitfragen und veröffentlichten Beiträge auf Open CoDE. Sie finden die Beiträge (="Issues") des FIT-ABs über die Seite [Issues](https://gitlab.opencode.de/bmi/ozg-rahmenarchitektur/zielbild-ozg-rahmenarchitektur/-/blob/main/Anleitung_Sichtung.png) (2). Um einen **Beitrag zu kommentieren** öffnen Sie das jeweilige "Issue" (3). Folgen Sie der Anleitung im nächsten Schritt.

> **Hinweis:** Die Veröffentlichung der Arbeitsstände zur Zielbilderarbeitung erfolgt durch das Arbeitspaket Zielbild OZG-Rahmenarchitektur des FIT-ABs. Dabei werden für jedes Arbeitsergebnis, Informationsveranstaltung und Leitfrage separate Issues erstellt.

### Schritt 2: Beteiligung

![Issue](Anleitung_Issue_Kommentierung.png)

In den jeweiligen Beiträgen und den **Beschreibungen** der Issues (4) finden Sie **relevante Informationen und Dokumente** (5). Sie können selbst **Dateien hochladen** (6), die sie beispielsweise ergänzen. 

> **Hinweis:** Veröffentlichen Sie Ihre Impulse und Rückfragen in **einem Kommentar** unter den jeweiligen Issues **im Namen Ihrer zu vertretenden Organisation** (7). Es ist besonders hilfreich, wenn Rückmeldungen zu den jeweiligen Beiträgen vor den Arbeitsterminen des FIT-ABs erfolgen. 

## Häufig gestellte Fragen

Auf unserer Webseite finden Sie Antworten zu häufig gestellten Fragen: [FAQ](https://bmi.usercontent.opencode.de/ozg-rahmenarchitektur/zielbild-ozg-rahmenarchitektur/faq/#gibt-es-fur-die-beantwortung-der-leitfragen-jeweils-eine-festgelegte-frist)


### ℹ️ 
**Bei weiteren Fragen zur Konsultation kontaktieren Sie uns gerne über unser Postfach: [Postfach OZG-Rahmenarchitektur](mailto:OZG-Rahmenarchitektur@bmi.bund.de)**
