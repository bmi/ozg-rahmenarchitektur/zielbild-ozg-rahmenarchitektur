import gitlab
import os
from datetime import datetime
import pytz

# Configuration
GITLAB_URL = 'https://gitlab.opencode.de' 
ACCESS_TOKEN =  os.getenv("ACCESS_TOKEN")
PROJECT_ID = '1557' 


# Initialize GitLab connection
gl = gitlab.Gitlab(GITLAB_URL, private_token=ACCESS_TOKEN)
project = gl.projects.get(PROJECT_ID)

# Fetch issues and comments
issues = project.issues.list(all=True)

issue_md_path = "website/docs/issues_raw.md"

def convert_to_german_timezone(datetime_string):
    datetime_object_utc = datetime.strptime(datetime_string, "%Y-%m-%dT%H:%M:%S.%fZ")
    utc_timezone = pytz.utc
    datetime_object_utc = utc_timezone.localize(datetime_object_utc)
    german_timezone = pytz.timezone('Europe/Berlin')
    datetime_object_german = datetime_object_utc.astimezone(german_timezone)
    formatted_datetime_german = datetime_object_german.strftime("%d.%m.%Y")
    return formatted_datetime_german

def initialien(eingabetext):
    formatierter_text = ''
    current_block = ''
    for char in eingabetext:
        if char.isupper():
            current_block += char
        elif current_block:
            formatierter_text += '.' + current_block
            current_block = ''
    if current_block:
        formatierter_text += '.' + current_block
    return formatierter_text[1:]

def isedited(issue_comment):
    if issue_comment['created_at'] != issue_comment['updated_at']:
         isedited_output = " - editiert am " + convert_to_german_timezone(issue_comment['updated_at'])
    else: isedited_output = " - original vom " + convert_to_german_timezone(issue_comment['created_at'])
    return isedited_output     

with open(issue_md_path, 'a') as file:
        file.write("\n" + "**Letzte Aktualisierung:** " + datetime.strptime(os.environ.get("CI_JOB_STARTED_AT"), '%Y-%m-%dT%H:%M:%SZ').strftime('%d.%m.%Y') + "\n"+ "\n")
        file.write("\n" + "---" +"\n"+ "\n")

label_list = []
for issue in issues:
    label_list.extend(issue.labels)

for label in ["Nutzendengruppen und Nutzendenreisen", "Strategische Leitplanken","Zielbild","Bausteine","IT-Architekturrichtlinie"]:
    with open(issue_md_path, 'a') as file:
        file.write("\n"+ "## " + str(label) + "\n"+ "\n")
    for issue in issues:
        if label in issue.labels and "Public" in issue.labels:
            notes = issue.notes.list(all=True)
            discussions = issue.discussions.list(all=True)
            comment_count = len(discussions)
            with open(issue_md_path, 'a') as file:
                file.write("??? info "+"\"" + str(issue.title) +"\""+ "\n"+ "\n")
                file.write("    " + "Datum der Veröffentlichung: " + convert_to_german_timezone(issue.created_at)+ "<br>")
                file.write("    " + "Anzahl eingegangener Antworten: " + str(comment_count)+ "<br>")
                file.write("    " + "Issue # in GitLab: " + "#" + str(issue.iid)+ "<br>")
                for line in issue.description.splitlines():
                            file.write("    " + str(line.replace('#',''))+ "\n")
                file.write("\n" + "    ---" + "\n")
            for id in discussions:
                notes = id.attributes['notes']
                if len(notes) > 1:
                    counter = 0
                    for i in notes:
                        counter += 1
                        if counter == 1:
                            with open(issue_md_path, 'a') as file:
                                file.write("    **" + initialien(i['author']['name'])+ "**"+ isedited(i) +"\n")
                                file.write("    > " + "\n")
                                for line in i['body'].splitlines():
                                    file.write("    > " + str(line.replace('#',''))+ "\n")
                            with open(issue_md_path, 'a') as file:
                                file.write("\n")
                                file.write("    > ---" + "\n")
                        else:
                            with open(issue_md_path, 'a') as file:
                                file.write("    > **" + initialien(i['author']['name']) + "**"+ isedited(i) +"\n")
                                file.write("    > > " + "\n")
                                for line in i['body'].splitlines():
                                    file.write("    > > " + str(line.replace('#',''))+ "\n")
                            with open(issue_md_path, 'a') as file:
                                file.write("\n")
                                file.write("    > ---" + "\n")
                    counter = 0
                if len(notes) == 1 and notes[0]["system"]==False:
                    with open(issue_md_path, 'a') as file:
                        file.write("    **" + initialien(notes[0]['author']['name']) + "**"+ isedited(notes[0]) +"\n")
                        file.write("    > " + "\n")
                        for line in notes[0]['body'].splitlines():
                            file.write("    > " + str(line.replace('#',''))+ "\n")

                        with open(issue_md_path, 'a') as file:
                                file.write("\n")
                                file.write("    ---" + "\n")
                
                if notes[0]["system"]==False:
                    with open(issue_md_path, 'a') as file:
                                    file.write("\n")
                                    file.write("    ---" + "\n")

