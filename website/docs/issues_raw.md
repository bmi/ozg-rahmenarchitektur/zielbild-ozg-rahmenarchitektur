In unserer Rubrik "Aktuelle Rückmeldungen" können Sie die Rückmeldungen zu den Leitfragen einsehen, die im Rahmen der ersten Phase des Konsultationsprozesses diskutiert wurden.

Wir waren bestrebt, eine transparente und offene Diskussion zu fördern und unsere Erkenntnisse sowie die eingegangenen Vorschläge und Kommentare zu den Leitfragen mit Ihnen auf dieser Seite zu teilen. Die eigentliche Online-Konsultation fand über Open CoDE statt. Die vom FIT-AB formulierten Leitfragen wurden dort von den Teilnehmenden des Konsultationsprozesses kommentiert.

Die Leitfragen sowie deren Rückmeldungen sind nach Themenschwerpunkten der Leitfragen auf dieser Seite strukturiert. Nur Leitfrage 12, welche das Thema der Open CoDE Kommentarfunktion thematisiert, wird hier nicht gespiegelt, da sie ledigleich den Impuls hatte, die Diskussion innerhalb Open CoDE anzuregen und auf bestehende Kommentare zu referenzieren. 

---

<br>