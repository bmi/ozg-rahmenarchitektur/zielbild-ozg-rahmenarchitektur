# Konsultationsprozess

## Ziele des Konsultationsprozesses
Der begleitende Konsultationsprozess zur Zielbilderarbeitung der OZG-Rahmenarchitektur förderte einen partizipativen Dialog mit verschiedenen Interessensgruppen sowie transparente Entscheidungsprozesse. Die Ergebnisse des Konsultationsprozesses sind kontinuierlich in die Zielbilderarbeitung eingeflossen – ein entscheidender Erfolgsfaktor für eine zukunftsfähige, effiziente, vertrauenswürdige und digital souveräne öffentliche Verwaltung Deutschlands. <br>

Des Weiteren verfolgt der Konsultationsprozess folgende Ziele: 

- **(1) Transparenz:** Offener Austausch zu den Visionen pro Funktionsbaustein durch die Konsultationsteilnehmenden auf Open CoDE.
- **(2) Aktive Beteiligung:** Die Teilnehmenden spielten eine entscheidende Rolle als Impulsgebende und kritische Meinungstragende zur Bewertung und Kommentierung der Ergebnisse in diesem Prozess. 
- **(3) Identifizierung potenzieller „blinder Flecken“:** Eine vollständige Informationsbasis wurde geschaffen, indem teilweise divergierende Perspektiven unterschiedlicher Interessengruppen eingeholt und berücksichtig wurden. 

## Erste Phase des Konsultationsprozesses

**Die erste Phase des Konsultationsprozesses basierte auf dem Vorhaben „Zielbild der OZG-Rahmenarchitektur“, welches gemeinsam durch BMI, FIT-AB und der Föderalen IT-Kooperation (FITKO) initiiert wurde.**

Eingebunden in den Konsultationsprozess waren 124 Beteiligte der vier Interessensgruppens Politik und Verwaltung, Wirtschaft, Wissenschaft sowie der Zivilgesellschaft. Insgesamt wurden 14 Leitfragen durch das FIT-AB entwickelt und anschließend auf der Plattform Open CoDE veröffentlicht. Durch diesen partizipativen Ansatz hatten die Teilnehmenden über den gesamten Zeitraum des Konsultationsprozesses hinweg die Möglichkeit, ihre Kommentare, Fragen und Empfehlungen zu den Leitfragen über Open CoDE einzureichen und sich auch untereinander auszutauschen. 
Die Entwicklung des Zielbilds im Rahmen des Vorhabens erfolgte iterativ (siehe Abbildung 1). Hierzu wurden die Rückmeldungen aus dem Konsultationsprozess durch das FIT-AB innerhalb des Vorhabens bewertet, und lieferten dadurch wichtige Impulse für den weiteren Prozess der Zielbildentwicklung.  Außerdem fanden während der ersten Phase des Konsultationsprozesses insgesamt drei Online-Informationsveranstaltungen statt, in denen das BMI den Teilnehmenden aktuelle Arbeitsstände präsentierte und die zur Klärung zur Verständnisfragen dienten.

## Zweite Phase des Konsultationsprozesses

**Im Mittelpunkt der zweiten Phase des Konsultationsprozesses stand die Entwicklung der Visionen pro Funktionsbaustein der OZG-Rahmenarchitektur.**

In der zweiten Phase des Konsultationsprozesses wurden die vom BMI eingebrachten Visionen des Zielbilds der OZG-Rahmenarchitektur validiert. Dabei beteiligten sich, wie bereits in der ersten Phase, Personen aus den Interessensgruppen Politik und Verwaltung, Wirtschaft, Wissenschaft sowie der Zivilgesellschaft. Als Austauschplattform und zentrale Stelle für das Einholen von Feedback diente erneut Open CoDE. Den Teilnehmerinnen und Teilnehmern standen über 60 Visionen in insgesamt 13 Funktionsbausteinen zur Bewertung offen. Dank der Beteiligung von mehr als 150 Akteurinnen und Akteuren und über 730 Kommentaren konnten wichtige Impulse für die Überarbeitung der Visionen pro Funktionsbaustein und die Zielbildentwicklung erlangt werden. 
