# Funktionsbausteine

Funktionsbausteine bündeln überschneidungsfrei und komponentenunabhängig gleichartige fachliche Funktionalitäten (z.B. Postfachfunktion) entlang generischer Verwaltungsprozesse. Sie sind in sinnvoller Beziehung zueinander auf vier Ebenen „Zugang für Bürgerinnen & Bürger sowie Organisationen & Unternehmen“, „Basisdienste“, „Zugang für die Verwaltung“ sowie „Entwicklung und Betrieb“ angeordnet.

Insgesamt gibt es **13 Funktionsbausteine**, welche sich wie folgt unterteilen lassen: 

**Zugang für Verwaltungskunden**

- Anliegensklärung
- Antragsinitiierung und -stellung

**Basisdienste**

- Identitätsmanagement
- Antragsbezogene Folgekommunikation
- Ein- und Auszahlung
- Siegeln und Signieren

**Zugang für die Verwaltung**

- Zustellung an Vorgangs- und Sachbearbeitung
- Vorgangsbearbeitung
- Abruf und Bereitstellung von Daten und Nachweisen

**Entwicklung und Betrieb**

- Kollaboration und Kommunikation unter Entwickelnden
- Entwicklung von Onlinediensten unter Entwickelnden 
- Beschaffung und Bereitstellung von Onlinediensten und Fachverfahren
- Betrieb von IT-Anwendungen / -Systemen

Übersicht zu den Funktionsbausteinen:

![Funktionsbausteine](<media/Übersicht Funktionsbausteine.png>)


## Visionen pro Funktionsbaustein 

**Die Visionen der OZG-Rahmenarchitektur geben eine klare Richtung für die weitere OZG-Umsetzung und Zusammenarbeit vor.**
Die Visionen befähigen Verwaltungsmitarbeitende, Entscheidungstragende sowie IT-Entwickelnde und Betreibende ihre Erwartungen abzugleichen, Entscheidungen im Einklang mit ihren eigenen langfristigen Zielen zu treffen sowie Akzeptanz für Veränderung zu schaffen. Zudem fördern die Visionen die Zusammenarbeit verschiedener Interessensgruppen und legen die Grundlage für die Standardisierung von Prozessen und Vorgehensweisen. Jede Vision der OZG-Rahmenarchitektur ist dabei einem spezifischen Funktionsbaustein zugeordnet. 

**Im Rahmen der Konsultation wurden insgesamt 60 Visionen in 13 Funktionsbausteinen definiert.** 
Ein erster Entwurf der Visionen wurde vom BMI zur Kommentierung auf Open CoDE veröffentlicht. Während der Kommentierungsphase haben die Teilnehmenden zahlreiche Vorschläge für neue Visionen, Anpassungen, Streichungen sowie Zusammenführungen der Visionen eingebracht. Die folgende Übersicht bietet einen Überblick über die entwickelten Visionen.

![Visionen pro Funktionsbaustein](<media/Visionen_pro_Funktionsbaustein.png>)