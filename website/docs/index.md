<div class="bpa-module bpa-module--dark-eagle"><div class="bpa-container"><div class="bpa-row"><div class="bpa-col"><h1 class="bpa-heading bpa-heading--h1">Projekt Zielbild OZG-Rahmenarchitektur <br/> Konsultation des FIT-AB</h1></div></div></div></div>
<br/>
!!! info "Was Sie erwartet"
    Gesammelte Informationen zur Zielsetzung, Rahmenbedingungen und Bewerbung zum Konsultationsprozess zur Zielbilderarbeitung der OZG-Rahmenarchitektur finden Sie hier. 


## Unser Projekt

Um den Ländervertretenden sowie Interessensgruppen aus Politik & Verwaltung, Wirtschaft, Wissenschaft sowie der Zivilgesellschaft die Möglichkeit zu geben, an dem Zielbild der OZG-Rahmenarchitektur mitzuwirken, wurde ein Konsultationsprozess in zwei Phasen durchgeführt.


## Ausgangslage und Zielsetzung

**Ziel des Onlinezugangsgesetzes (OZG) ist es, Verwaltungsleistungen für Bürgerinnen, Bürger, Unternehmen, Verwaltungsmitarbeitende und die Verwaltung selbst durch attraktive, nutzendenfreundliche digitale Angebote einfach, sicher und von überall und zu jedem Zeitpunkt verfügbar zu machen.** 

Mit der Verabschiedung des OZG-Änderungsgesetz rückte nun auch die beschleunigte Ende-zu-Ende Digitalisierung dre Verwaltung in den Fokus. In diesem Zusammenhang wird durch die Veröffentlichung des Zielbilds der OZG-Rahmenarchitektur ein gemeinsamer strategischer Rahmen geschaffen, der die verschiedenen Ansätze der bisherigen OZG-Umsetzung bündelt und als Entscheidungsgrundlage für eine zukunftsfähige und beschleunigte OZG-Umsetzung dient. Das Zielbild ist dabei auf der strategischen Ebene der föderalen IT-Architektur angesiedelt und setzt sich aus einzelnen Visionen zusammen, die für eine bessere Einordnung sowie Übersichtlichkeit Funktionsbausteinen zugeordnet werden.  Die Konkretisierung sowie Festlegung spezifischer Umsetzungsmaßnahmen des Zielbilds erfolgt durch die Etablierung von Governance-Strukturen und Vorgaben, die sich aus den einzelnen Visionen ableiten lassen. 

