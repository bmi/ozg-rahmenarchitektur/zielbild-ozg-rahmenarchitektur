# Frequently Asked Questions

## Allgemeines

??? info "Was ist der Konsultationsprozess?"
    ## Was ist der Konsultationsprozess? 
     Für die Erstellung des Zielbilds der OZG-Rahmenarchitektur initiierte das BMI bereits Ende 2023 das **Architekturvorhaben „Zielbild der OZG-Rahmenarchitektur“** im Föderalen IT-Architekturboard. Begleitend dazu fand die erste Phase eines **zweiphasigen Konsultationsprozesses** statt, in dessen Rahmen das Architekturvorhaben iterativ Arbeitsergebnisse in die Konsultation einbrachte. Der Konsultationsprozess unterstützte dabei, Transparenz und Zusammenarbeit zu fördern und gleichzeitig Vertrauen und Akzeptanz für das Zielbild der OZG-Rahmenarchitektur zu stärken. Die erstellten Inhalte der ersten Phase der Konsultation dienten als Grundlage für die Erarbeitung von einzelnen Visionen. Im Mittelpunkt der zweiten Phase des Konsultationsprozesses stand dann die Entwicklung von Visionen pro Funktionsbaustein der OZG-Rahmenarchitektur. Die vom BMI eingebrachten Visionen pro Funktionsbaustein zum Zielbild der OZG-Rahmenarchitektur wurden von über 150 Teilnehmenden aus Politik und Verwaltung, Wirtschaft, Wissenschaft sowie der Zivilgesellschaft über die Austauschplattform Open CoDE validiert und weiterentwickelt. CoDE** statt. Durch das FIT-AB formulierte Leitfragen können dort von Teilnehmenden des Konsultationsprozesses (Bewerbende) kommentiert werden. Alle Beiträge sind öffentlich einsehbar.

??? info "Was ist die OZG-Rahmenarchitektur?" 
    ## Was ist die OZG-Rahmenarchitektur? 
     **Ziel des Onlinezugangsgesetzes (OZG) ist es, Verwaltungsleistungen für Bürgerinnen, Bürger, Unternehmen, Verwaltungsmitarbeitende und die Verwaltung selbst durch attraktive, nutzendenfreundliche digitale Angebote einfach, sicher und von überall und zu jedem Zeitpunkt nutzbar zu machen.**
     Maßgeblich dafür ist ein Zielbild für eine ganzheitliche OZG-Architektur, welche einen gemeinsamen Rahmen für die Verwaltungsdigitalisierung von Bund und Ländern definieren soll. Diese sogenannte "OZG-Rahmenarchitektur" schafft einen strategischen Rahmen als Zielausrichtung für eine ineinandergreifende OZG Umsetzung. Das Zielbild wird produktneutral erarbeitet.
     
??? info "Was sind die nächsten Schritte?"
    ## Was sind die nächsten Schritte?
       Im Anschluss an die zweite Phase des Konsultationsprozesses werden die Ergebnisse konsolidiert. Auf Grundlage der überarbeiteten Visionen wird im nächsten Schritt ein **übergreifendes Zielbild für die OZG-Rahmenarchitektur** entworfen.