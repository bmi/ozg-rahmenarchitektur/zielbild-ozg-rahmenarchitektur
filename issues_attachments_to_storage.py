import re
import subprocess
import os

def replace_between_first_square_bracket_and_last_parentheses(input_string, replacement_text):
    pattern = re.compile(r'\[.*?\].*?\)', re.DOTALL)
    match = pattern.search(input_string)

    if match:
        result = input_string[:match.start()] + replacement_text + input_string[match.end():]
        return result
    else:
        return input_string

def extract_links(markdown_text):
    links = re.findall(r'\[.*?\]\((.*?)\)', markdown_text)
    filtered_links = [link for link in links if "/uploads/" in link]
    return filtered_links

with open("website/docs/issues_raw.md", 'r', encoding='utf-8') as file:
    markdown_text = file.read()

links = extract_links(markdown_text)
fixed_pairs = []
link_list = []
for link in links:
    filename = link.split("/")[-1]
    curl_command = ['curl', "-J", "-o", "./website/docs/assets/"+filename,"--header", "PRIVATE-TOKEN: " + os.getenv("ACCESS_TOKEN"), "https://gitlab.opencode.de/bmi/ozg-rahmenarchitektur/zielbild-ozg-rahmenarchitektur" + link]
    subprocess.run(curl_command)
    filename_notype = filename.split(".")[0]
    path_fix = "[" + filename_notype + "]"+"(./assets/" + filename + ")"
    fixed_pairs.append([link, path_fix])
    link_list.append(link)

with open("website/docs/issues_raw.md", 'r') as file, open("website/docs/issues.md", 'w') as output_file:
    for line in file:
        overwrite = False
        for i in fixed_pairs:
            if i[0] in line:
                overwrite = True
                new_line_content = replace_between_first_square_bracket_and_last_parentheses(line, i[1])
        if overwrite == True:
            output_file.write(new_line_content)
        if overwrite == False:
            output_file.write(line)
        