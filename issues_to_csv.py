# This is a simple script that exports the Issues (and comments) of the project to CSV.
# This is a necessary workaround as the issue-csv-export of gitlab omits comments.

import gitlab
import csv
import os

# Configuration
GITLAB_URL = 'https://gitlab.opencode.de' 
ACCESS_TOKEN =  os.getenv("ACCESS_TOKEN")
PROJECT_ID = '1557' 


# Initialize GitLab connection
gl = gitlab.Gitlab(GITLAB_URL, private_token=ACCESS_TOKEN)
project = gl.projects.get(PROJECT_ID)

# Fetch issues and comments
issues = project.issues.list(all=True)

# Prepare CSV file
with open('output/issues_and_comments.csv', 'w', newline='') as csvfile:
    csv_writer = csv.writer(csvfile)
    csv_writer.writerow(['Issue Identifier', 'Issue Name', 'Unique (upstream) Comment-Identifier', 'Comment- or Issue-Text', 'Name of commentator', 'time-of-publication', 'time-of-update'])
    
    for issue in issues:
        # Write issue details to CSV
        csv_writer.writerow([issue.id, issue.title, None,  issue.description, issue.author["name"], issue.created_at, issue.updated_at])

        # Fetch notes (comments) for the issue
        notes = issue.notes.list(all=True)
        for note in notes:
            # Only consider comments (notes can also be system notes, like status changes)
            if not note.system:
                csv_writer.writerow([issue.id, issue.title, note.id, note.body, note.author["name"], note.created_at, issue.created_at])

print("CSV export completed!")
